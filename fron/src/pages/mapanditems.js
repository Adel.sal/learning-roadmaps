import React, { Component } from 'react';
import { pause, makeRequestUrl } from "../uutils.js";
import { ToastContainer, toast } from "react-toastify";






const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);



class mapanditems extends Component {

  state={
    users_list:[],
    maps_list:[],
    map_title: "",
    map_description: "",
    map_public: "",
    map_image: "",
    user_profile_user_id: "",
    map_fork_id: "",
    map_tags: "",
    item_title: "",
    item_description: "",
    item_image: "",
    item_checked: "",
    learn_map_map_id: "",
    error_message: "", 
    isLoading:false

  }

  // **************** MAPS FETCHING ***********************
    // {/*CREATING NEW MAP */}

  createnewmap = async props => {
    try {
      if(!props || !props.map_title || !props.map_description || !props.map_tags || !props.user_profile_user_id) {
        throw new Error('you must provide a title and a description and tags');
      }
      const {map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags } = props;
      let body = null;

      if(map_image){
        body = new FormData();
        body.append(`image`, map_image)
      }


      const response = await fetch(
        `http://localhost:8080/maps/new/?map_title=${map_title}&map_description=${map_description}&map_public=${map_public}&image=${map_image}&user_profile_user_id=${user_profile_user_id}&map_fork_id=${map_fork_id}&map_tags=${map_tags}`,
        {
          method:'POST',
          body
        }
      );
      const answer = await response.json();
      if (answer.success){
        const id = answer.result;
        const map = {map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags, id};
        const maps_list = [...this.state.maps_list, map]
        this.setState({ maps_list });
        toast(`member "${map_title}" created`);

      } else {
        this.setState({error_message: answer.message })
}

  } catch (err) {
    this.setState({ error_message: err.message });

}
};


//{/* VIEW THE MAP */ }

viewmap = async id => {
  const previous_map = this.state.maps_list.find(
    map => map.id === id
  );
  if (previous_map) {
    return
  }
  try {
    const response = await fetch(`http://localhost:8080/maps/view/${id}`);
    const answer = await response.json();
    if (answer) {
      const map = answer.result;
      const maps_list = [...this.state.maps_list, map];
      this.setState({ maps_list });
      toast(`map loaded`);

    }
    else {
      this.setState({ error_message: answer.message });
      toast.error(answer.message);

    }
} catch (err) {
  this.setState({ error_message: err.message });
  toast.error(err.message); // <--- show a toaster


}
};

// { /* UPDATE THE MAP */ }

updatemap = async (id, props) => {
  try {
    if (!props || !(props.map_title || props.map_description || props.map_tags || props.user_profile_user_id )) {
      throw new Error(
        `you need at least title and description and tags properties to update a map`
      );
    }
  const url = makeUrl(`maps/update/${id}`, {
    map_title: props.map_title,
    map_description: props.map_description,
    map_public: props.map_public,
    map_image: props.map_image,
    user_profile_user_id: props.user_profile_user_id,
    map_fork_id: props.map_fork_id,
    map_tags:props.map_tags
  });

  let body = null;
  if(props.image){
    body = new FormData();
    body.append(`image`, props.image)
  }        


  const response = await fetch(url, {
    method:'POST', 
    body });
  const answer = await response.json();
  if (answer.success) {

    const maps_list = this.state.maps_list.map(map => {


      if (map.id === id) {
        const new_map = {
          id: map.id,
          title: props.map_title || map.map_title,
          description: props.map_description || map.map_description,
          public: props.map_public || map.map_public,
          image: props.map_image || map.map_image,
          user_profile_user_id: props.user_profile_user_id || map.user_profile_user_id,
          forked: props.map_fork_id || map.map_fork_id,
          tags: props.map_tags || map.map_tags,

          };
          
        return new_map;
      }

      else {
        return map;
      }
    });
    this.setState({ maps_list });
  } else {
    this.setState({ error_message: answer.message });
    toast.error(answer.message);

  }
} catch (err) {
  this.setState({ error_message: err.message });
  toast.error(err.message);

}
};  


// SET MAP TO PUBLIC
setmaptopublic = async (id, props) => {
  try {
    if (!props || !(props.map_title || props.map_description || props.map_tags || props.user_profile_user_id )) {
      throw new Error(
        `you need at least title and description and tags properties to update a map`
      );
    }
  const url = makeUrl(`maps/update/${id}`, {
    map_public: props.map_public,
  });

  const response = await fetch(url, {
    method:'POST', 
     });
  const answer = await response.json();
  if (answer.success) {

    const maps_list = this.state.maps_list.map(map => {


      if (map.id === id) {
        const new_map = {
          id: map.id,
          public: props.map_public || map.map_public,
          };
          
        return new_map;
      }

      else {
        return map;
      }
    });
    this.setState({ maps_list });
  } else {
    this.setState({ error_message: answer.message });
  }
} catch (err) {
  this.setState({ error_message: err.message });
}
};  


// DELETE MAP

deletemap = async id => {
  try {
    const response = await fetch(
      `http://localhost:8080/maps/delete/${id}`
    );
    const answer = await response.json();
    if (answer.success) {
      // remove the map from the current list of maps
      const maps_list = this.state.maps_list.filter(
        map => map.id !== id
      );
      this.setState({ maps_list });
      toast(`member deleted`);

    } else {
      this.setState({ error_message: answer.message });
      toast.error(answer.message);

    }
  } catch (err) {
    this.setState({ error_message: err.message });
    toast.error(err.message); // <--- show a toaster
  }
};


// LIST OF MAPS

getmapslist = async order => {
  this.setState({ isLoading: true });
  try {
    const response = await fetch(
      `http://localhost:8080/maps/list?order=${order}`
    );
    await pause();
    const answer = await response.json();
    if (answer.success) {
      const maps_list = answer.result;
      this.setState({ maps_list, isLoading: false });
      toast("maps loaded"); // <--- Show a toaster

    } else {
      this.setState({ error_message: answer.message, isLoading: false });
      toast.error(answer.message); // <--- show a toaster

    }
  } catch (err) {
    this.setState({ error_message: err.message, isLoading: false });
  }
};

// * Fork map 

mapfork = async id => {
  const forked_map = this.state.maps_list.find(
    map => map.id === id
  );
  if (forked_map) {
    return
  }
  try {
    const response = await fetch(`http://localhost:8080/maps/fork/${id}`);
    const answer = await response.json();
    if (answer) {
      const map = answer.result;
      const maps_list = [...this.state.maps_list, map];
      this.setState({ maps_list });
        
    }
    else {
      this.setState({ error_message: answer.message });
    }
} catch (err) {
  this.setState({ error_message: err.message });

}
};




// ******************************* ITEMS FETCHING ******************************

// * CREATE NEW ITEM

createnewitem = async props => {
  try {
    if(!props || !props.item_title || !props.item_description || !props.item_image) {
      throw new Error('you must provide a title and a description and tags');
    }
    const {item_title, item_description, item_checked, item_image, user_profile_user_id, learn_map_map_id } = props;
    let body = null;

    if(item_image){
      body = new FormData();
      body.append(`image`, item_image)
    }

      
    const response = await fetch(
      `http://localhost:8080/mapsitem/new/?item_title=${item_title}&item_description=${item_description}&item_checked=${item_checked}&iteimage=${item_image}&user_profile_user_id=${user_profile_user_id}&learn_map_map_id=${learn_map_map_id}`,
      {
        method:'POST',
        body
      }

      );
    const answer = await response.json();
    if (answer.success){
      const id = answer.result;
      const mapitem = {item_title, item_description, item_checked, item_image, user_profile_user_id, learn_map_map_id, id};
      const items_list = [...this.state.items_list, mapitem]
      this.setState({ items_list });
      toast(`item "${item_title}" created`);


    } else {
      this.setState({error_message: answer.message })
}

} catch (err) {
  this.setState({ error_message: err.message });

}
};


//  VIEW ITEMS

viewitem = async id => {
  const previous_mapitem = this.state.items_list.find(
    mapitem => mapitem.id === id
  );
  if (previous_mapitem) {
    return
  }
  try {
    const response = await fetch(`http://localhost:8080/mapitem/view/${id}`);
    const answer = await response.json();
    if (answer) {
      const map = answer.result;
      const items_list = [...this.state.items_list, map];
      this.setState({ items_list });
     toast(`map loaded`);

    }
    else {
      this.setState({ error_message: answer.message });
      toast.error(answer.message);

    }
} catch (err) {
  this.setState({ error_message: err.message });

}
};

// UPDATE ITEM

updatitem = async (id, props) => {
  try {
    if (!props || !(props.item_title || props.item_description || props.item_checked )) {
      throw new Error(
        `you need at least title or description properties to update an item`
      );
    }
  const url = makeUrl(`mapitem/update/${id}`, {
    item_title: props.map_title,
    item_description: props.item_description,
    item_checked: props.item_checked,
    item_image: props.item_image,
    user_profile_user_id: props.user_profile_user_id,
    learn_map_map_id: props.learn_map_map_id
  });

  let body = null;
  if(props.image){
    body = new FormData();
    body.append(`image`, props.image)
  }        

  const response = await fetch(url, {
    method:'POST', 
     });
  const answer = await response.json();
  if (answer.success) {

    const items_list = this.state.items_list.map(mapitem => {


      if (mapitem.id === id) {
        const new_item = {
          id: mapitem.id,
          title: props.item_title || mapitem.item_title,
          description: props.item_description || mapitem.item_description,
          public: props.item_checked || mapitem.item_checked,
          image: props.item_image || mapitem.item_image,
          user_profile_user_id: props.user_profile_user_id || mapitem.user_profile_user_id,
          learn_map_map_id: props.learn_map_map_id || mapitem.learn_map_map_id,

          };
          
        return new_item;
      }

      else {
        return mapitem;
      }
    });
    this.setState({ items_list });
  } else {
    this.setState({ error_message: answer.message });
    toast.error(answer.message);

  }
} catch (err) {
  this.setState({ error_message: err.message });
  toast.error(err.message);

}
};  

// DELETE ITEM

deleteitem = async id => {
  try {
    const response = await fetch(
      `http://localhost:8080/mapitem/delete/${id}`
    );
    const answer = await response.json();
    if (answer.success) {
      // remove the iten from the current lists of items
      const items_list = this.state.items_list.filter(
        mapitem => mapitem.id !== id
      );
      this.setState({ items_list });
      toast(`member deleted`);

    } else {
      this.setState({ error_message: answer.message });
    }
  } catch (err) {
    this.setState({ error_message: err.message });
    toast.error(err.message); // <--- show a toaster

  }
};


// LIST OF ITEMS

getitemsList = async order => {
  this.setState({ isLoading: true });
  try {
    const response = await fetch(
      `http://localhost:8080/mapitem/list?order=${order}`
    );
    await pause();
    const answer = await response.json();
    if (answer.success) {
      const items_list = answer.result;
      this.setState({ items_list, isLoading: false });
      toast("items loaded"); // <--- Show a toaster

    } else {
      this.setState({ error_message: answer.message, isLoading: false });
      toast.error(answer.message); // <--- show a toaster

    }
  } catch (err) {
    this.setState({ error_message: err.message, isLoading: false });
    toast.error(err.message); // <--- show a toaster

  }
};

// *********************** RENDER ****************************************





componentDidMount() {
  this.getmapslist();
}
onSubmit = (evt) => {
  // stop the form from submitting:
  evt.preventDefault();

  // extract name and position and image from state
  const { map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags } = this.state;
  // create the member form name and position and image
  this.createnewmap({ map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags });
  // empty name and position and image so the text input fields are reset
  this.setState({ map_title: "", map_description: "", map_public: "", map_image: "", user_profile_user_id: "", map_fork_id: "", map_tags: ""  });
};




  render() {
    return (
      <div>
        
      </div>
    );
  }
}

export default mapanditems;