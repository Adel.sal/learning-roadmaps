import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { withRouter, Switch, Route, Link } from "react-router-dom";
import Maps from './components/maps';
import usersignup from './components/usersignup';
import profilepage from './components/profilepage';
import singlemap from './components/singlemap';
import test from './components/test';
import createmap from './components/createmap';


class App extends Component {
  render() {
    
    return (
      <Switch>
    <Route exact path="/" component={Maps}/>
    <Route exact path="/profilepage" component={profilepage}/>
    <Route exact path="/signup" component={usersignup}/>
    <Route exact path="/mapview" component={singlemap}/>
    <Route exact path="/test" component={test}/>
    <Route exact path="/createmap" component={createmap}/>





      </Switch>
    );
  }
}

export default withRouter(App)
