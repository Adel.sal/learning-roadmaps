import React, { Component } from 'react';
import './../styles/test.css';
import Footer from './Footer'
import Header from './Header'
import Roadmapslogo from '../logo/./Roadmapslogo.png'

class test extends Component {
  state={
    maps_list:[],
   
  };


  getmaplist = async order => {
    try {
      const response = await fetch(
        `http://localhost:8080/maps/list?order=${order}`
      );
  
      const answer = await response.json();
      if (answer) {
        const maps_list = answer.result;
        this.setState({ maps_list });
  
      } else {
        this.setState({ error_message: answer.message });
  
      }
    } catch (err) {
      this.setState({ error_message: err.message });
  
    }
  };

  componentDidMount() {
    //this.getmemberslist();
    this.getmaplist();
  }

  
  render() {

  
    return (
      <div>
        <Header></Header>
        <title>Visualize by TEMPLATED</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
        {/* Wrapper */}
        <div id="wrapper">
          {/* Header */}
          <header id="header">
            <span className="avatar">
            <img src="https://cdn1.vectorstock.com/i/1000x1000/31/95/user-sign-icon-person-symbol-human-avatar-vector-12693195.jpg" alt /></span>
            <h1>This is <strong>Visualize</strong>, a responsive site template designed by <a href="http://templated.co">TEMPLATED</a><br />
              and released for free under the Creative Commons License.</h1>
            <ul className="icons">
              <li><a href="#" className="icon style2 fa-twitter"><span className="label">Twitter</span></a></li>
              <li><a href="#" className="icon style2 fa-facebook"><span className="label">Facebook</span></a></li>
              <li><a href="#" className="icon style2 fa-instagram"><span className="label">Instagram</span></a></li>
              <li><a href="#" className="icon style2 fa-500px"><span className="label">500px</span></a></li>
              <li><a href="#" className="icon style2 fa-envelope-o"><span className="label">Email</span></a></li>
            </ul>
          </header>


        {/* Main */}
        <div id="main">
          <div className="inner">
            {/* Boxes */}
            <div className="thumbnails">
              <div className="box">
              <a href="/" className="image fit"><img src="https://cdn-images-1.medium.com/max/1200/1*y6C4nSvy2Woe0m7bWEn4BA.png" alt /></a>
                <div className="inner">
                  <h3>React Bigenners Steps</h3>
                  <p>Interdum amet accumsan placerat commodo ut amet aliquam blandit nunc tempor lobortis nunc non. Mi accumsan.</p>
                  <a href="/" className="button fit" >view</a>
                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img className="" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPv0eMrqMP6Y5TzBNM794lhpzhBTQEMv26b9p0EsFsBICJl2Xw" alt /></a>
                <div className="inner">
                  <h3>Nascetur nunc varius commodo</h3>
                  <p>Interdum amet accumsan placerat commodo ut amet aliquam blandit nunc tempor lobortis nunc non. Mi accumsan.</p>
                  <a href="/" className="button fit" >view</a>
                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img src="https://d1aeri3ty3izns.cloudfront.net/media/11/119190/1200/preview.jpg" alt /></a>
                <div className="inner">
                  <h3>Nascetur nunc varius commodo</h3>
                  <p>Interdum amet accumsan placerat commodo ut amet aliquam blandit nunc tempor lobortis nunc non. Mi accumsan.</p>
                  <a href="/" className="button fit" >view</a>
                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img src="https://cdn.app.compendium.com/uploads/user/e7c690e8-6ff9-102a-ac6d-e4aebca50425/9f78fc09-faec-4068-82bd-09e7cc8bbf34/File/e19ea0216ae8395bd4b3389970928be9/java_logo.png" alt /></a>
                <div className="inner">
                  <h3>Nascetur nunc varius commodo</h3>
                  <p>Interdum amet accumsan placerat commodo ut amet aliquam blandit nunc tempor lobortis nunc non. Mi accumsan.</p>
                  <a href="/" className="button fit" >view</a>
                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQQ0tAiKVhv0iiGkxZNu2dDzyH5vbt3curAlZAn7ph_WFzJB--" alt /></a>
                <div className="inner">
                  <h3>Nascetur nunc varius commodo</h3>
                  <p>Interdum amet accumsan placerat commodo ut amet aliquam blandit nunc tempor lobortis nunc non. Mi accumsan.</p>
                  <a href="/" className="button fit" >view</a>
                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img src="https://cdn-images-1.medium.com/max/1200/1*mYnT8uj0ukTlFY_1zP-bWQ.png" alt /></a>
                <div className="inner">
                  <h3>Nascetur nunc varius commodo</h3>
                  <p>Interdum amet accumsan placerat commodo ut amet aliquam blandit nunc tempor lobortis nunc non. Mi accumsan.</p>
                  <a href="/" className="button fit" >view</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer ></Footer>

      </div>
      </div>
    );
  }
}
 


export default test;