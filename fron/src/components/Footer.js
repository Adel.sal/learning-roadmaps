import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import '../styles/footer.css';



class Footer extends Component {
  render() { 
    return (

      <footer className="footer-distributed">

			<div className="footer-left">

				<h3><img className="footerlogo" src={require('../logo/WithS.png')}></img></h3>

				{/* <p className="footer-links">
				<Link to="/">Home</Link>
					-
					<Link to="/blogs">Our Blog</Link>
					-
					<Link to="/members">Members</Link>
					-
					<Link to="/about">About</Link>					
					-
				</p> */}

				<p className="footer-company-name">Learning Roadmaps &copy; 2019</p>
			</div>
			<div className="footer-center">

				<div>
					<i className="fa fa-map-marker"></i>
					<p><br /> Beirut, Lebanon</p>
				</div>

				<div>
					<i className="fa fa-phone"></i>
					<p>+961 7035226</p>
				</div>

				<div>
					<i className="fa fa-envelope"></i>
					{/* <p><a href="mailto:support@company.com">info@learningroadmaps.com</a></p> */}
				</div>

			</div>

			<div className="footer-right">

				<p className="footer-company-about">
					<span>About Learning Roadmaps</span>
Is an online platforms that helps learners to create their learning roadmaps as well as share it with other people and view / use others roadmaps and find the most suitable way to plan you learning journey.
				</p>

				<div className="footer-icons">

					{/* <a href="https://www.facebook.com/26letterslebanon/"><i className="fa fa-facebook"></i></a>
					<a href="#"><i className="fa fa-twitter"></i></a>
					<a href="#"><i className="fa fa-linkedin"></i></a> */}

				</div>

			</div>

		</footer>


    );
  }
}

    export default Footer;
