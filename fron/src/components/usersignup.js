import React, { Component } from 'react';
import Header from './Header';
import Footer from './Footer';


class usersignup extends Component {
  render() {
    return (
      <div>
        <Header />
        <br />
        <br />
        <br />
        <div>

        <form className="cf">
        <h1 className="formheader">Create your profile</h1>

          <div className="half left cf">
          <p>Your Name:</p>
            <input type="text" id="input-name" placeholder="Name" />
            <p>E-mail:</p>
            <input type="email" id="input-email" placeholder="Email address" />
            <p>Short Bio</p>
            <div className="half right cf">
            <textarea name="message" type="text" id="input-message" placeholder="Short Bio" defaultValue={"This is my bio"} />
          </div>  
            <p>Twitter Account</p>
            <input type="text" id="input-subject" placeholder="user_twitter" />
            <p>Facebook Profile</p>
            <input type="text" id="input-subject" placeholder="user_facebook" />
           <p>Linkedin Account</p>
            <input type="text" id="input-subject" placeholder="user_linkedin" />

          </div>
          {/* <div className="half right cf">
            <textarea name="message" type="text" id="input-message" placeholder="Short Bio" defaultValue={"This is my bio"} />
          </div>   */}
          <input type="submit" defaultValue="Submit" id="input-submit" />
        </form>
        <Footer />
      </div>
      </div>
    );
  }
}
export default usersignup;