import React, { Component } from 'react';
import './../styles/profilepage.css';
import Footer from './Footer'
import Header from './Header'
import { Link } from 'react-router-dom'


class profilepage extends Component {
  state={
    maps_list:[],
   
  };


  getmaplist = async order => {
    try {
      const response = await fetch(
        `http://localhost:8080/maps/list?order=${order}`
      );
  
      const answer = await response.json();
      if (answer) {
        const maps_list = answer.result;
        this.setState({ maps_list });
  
      } else {
        this.setState({ error_message: answer.message });
  
      }
    } catch (err) {
      this.setState({ error_message: err.message });
  
    }
  };

  componentDidMount() {
    //this.getmemberslist();
    this.getmaplist();
    window.scrollTo(0, 0)
  }

  
  render() {

  
    return (
      <div>
        <Header></Header>
        <header></header>
            <span className="avatar"> 
             <img  src="https://scontent.fbey18-1.fna.fbcdn.net/v/t1.0-9/50905740_1075987459259005_4037782850429255680_n.jpg?_nc_cat=104&_nc_ht=scontent.fbey18-1.fna&oh=a1f5aebd3c81d9f82daee77beb8a4997&oe=5D039D6B" alt />
          </span>
          <h1 className="username">Yazid Krayem</h1>

            <h1 className="usertext">Hi my name is Yazid am a full stack web developer passionate about technology and learning in general</h1>  
          {/* <div>
              <div class="avatarz"></div>
              <h1 class="username">Gianluca Pirrera</h1>
          </div>   



        {/* Main */}
        <div id="main">
          <div className="inner">
            {/* Boxes */}
            <div className="thumbnails">
            <div className="box">
              <a href="/" className="image fit"><img src="https://cms-assets.tutsplus.com/uploads/users/988/posts/31255/image/What-is-public-speaking%20(1).jpg" alt /></a>
                <div className="inner">
                  <h3>Want To Be A Better Public Speaker?</h3>
                  <p className="cardtext">

                  Do you dread public speaking? Join the club. Along with death and spiders, it's what people fear most. However, being an effective presenter is critical for anyone who is (or aspires to be) in a leadership position.
                      </p>             
                  
                  <Link className="button fit" to='./mapview' >view</Link>

                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img src="https://d1aeri3ty3izns.cloudfront.net/media/11/119190/1200/preview.jpg" alt /></a>
                <div className="inner">
                  <h3>Simple Steps to play guitar</h3>
                  <p className="cardtext">
                  For some players, learning to play guitar by themselves is really the only way to go. 
                  Guitar lessons have their place, and there is no substitute for an experienced teacher or mentor who is committed to helping you reach your goals. 
                  </p>
                  <br/>
                  <Link className="button fit" to='./mapview' >view</Link>
                </div>
              </div>
              
            </div>
          </div>
        </div>
        <Footer ></Footer>

      </div>
    );
  }
}
 


export default profilepage;