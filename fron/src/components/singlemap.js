import React, { Component } from 'react';
import './../styles/singlemap.css';
import '../components/Header'
import Header from '../components/Header';
import Container from './Container';
import Footer from '../components/Footer';
import { Link } from 'react-router-dom'



class singlemap extends Component {
  state={
    click:false
  }

  viewmap = async id => {
    const previous_map = this.state.maps_list.find(
      map => map.id === id
    );
    if (previous_map) {
      return
    }
    try {
      const response = await fetch(`http://localhost:8080/maps/view/${id}`);
      const answer = await response.json();
      if (answer) {
        const map = answer.result;
        const maps_list = [...this.state.maps_list, map];
        this.setState({ maps_list });
  
      }
      else {
        this.setState({ error_message: answer.message });
  
      }
  } catch (err) {
    this.setState({ error_message: err.message });
  
  
  }
  };
  



  componentDidMount() {
    //this.getmemberslist();
    this.viewmap();
    window.scrollTo(0, 0)
  }

  render() {
    
    return (
      <div>
      <Header />

        <section className="timeline">

        <div>
            <h1 className="maptitle">Want To Be A Better Public Speaker? Do What The Pros Do.</h1>
        </div>
        <br />
        <br />
        <div>
        </div>

        <div className="imageitem">
              <img src="https://cms-assets.tutsplus.com/uploads/users/988/posts/31255/image/What-is-public-speaking%20(1).jpg" alt />
        </div>
          <ul className="mapitemslist">
            <li className="in-view">
              <div className="reveal">
                <p>
                  <phase>Begin with the end in mind.</phase> 
                    <p>Before you start working on your script or presentation, get clear on its purpose.
                       What are you trying to accomplish? What impact do you want to have on your audience? Are you looking to inform? Inspire? Persuade? Knowing your ultimate purpose and desired outcome will help you stay focused through the preparation process.
                    </p>  
                          <input className="checkb" type="checkbox" id="checkbox" />
                          <label for="checkbox"></label>
                 </p>
              </div>
            </li>
            <li className="in-view">
              <div className="reveal">
                <p>
                  <phase>Simplify your messages. </phase> 
                   <p>TYou are where you are because of the depth and breadth of your expertise.
                     Your natural inclination will be to impart lots of that knowledge onto your audience. Resist it! Otherwise, you'll bore and overwhelm your listeners with details they’ll never retain.
                     Focus on conveying a few powerful ideas that they’ll remember. 
                     Think of yourself as Master Distiller of Information – boil it down and go from there. 
                   </p>
                   <input className="checkb" type="checkbox" id="checkbox" />
                  <label for="checkbox"></label>

                </p>
              </div>
            </li>
            <li className="in-view">
              <div className="reveal">
                <p>
                  <phase>Avoid the perils of Powerpoint. </phase>
                    <p>
                      When you speak, try to engage your audience. This makes you feel less isolated as a speaker and keeps everyone involved with your message. 
                      If appropriate, ask leading questions targeted to individuals or groups, and encourage people to participate and ask questions.
                      Keep in mind that some words reduce your power as a speaker. For instance, think about how these sentences sound: "I just want to add that I think we can meet these goals" or "I just think this plan is a good one.
                      " The words "just" and "I think" limit your authority and conviction. Don't use them.
                    </p> 
                    <input className="checkb" type="checkbox" id="checkbox" />
                          <label for="checkbox"></label>

                </p>
              </div>
            </li>
            <li className="in-view">
              <div className="reveal">
                <p>
                  <phase>Connect with your audience.</phase>
                    <p>
                    One mistake speakers often make is trying to prove they’re smart. 
                    Remember that you're at the podium for a reason. Your credentials speak for themselves. When you stand in front of an audience, there is already a gap -- you’re the expert, they’re not. By trying to impress your audience with your intellect, you create more distance and could come across as arrogant. Your job is to close the gap, not widen it. 
                    By being self-effacing, humorous and real, you become approachable and it’s easier to win over your audience. In turn, the more connected the audience feels to you, the more they’ll pay attention to what you have to say.    
                    </p>
                    <input className="checkb" type="checkbox" id="checkbox" />
                    <label for="checkbox"></label>

                </p>
              </div>
            </li>
            <li className="in-view">
              <div className="reveal">
                <p>
                  <phase>Tell personal stories.</phase> 
                 <p> Storytelling puts an audience at ease, humanizes you as a speaker, and makes your messages more memorable.
                   It is the most powerful tool in a speaker’s toolkit.  To find your stories, you simply have to mine your own life experiences and pull out the gems. Audiences will remember your stories more easily than facts and figures, and they're more likely to enjoy your presentation. 
                  Another benefit is that personal stories are easier for you to remember when you're at the podium. 
                  </p>
                  <input className="checkb" type="checkbox" id="checkbox" />
                  <label for="checkbox"></label>
                </p>
              </div>
            </li>
            <li className="in-view">
              <div className="reveal">
                <p>
                  <phase>Prepare and practice.</phase> 
                  <p>If you're giving a high-stakes presentation, don’t leave anything to chance. 
                  “Off the Cuff” and “Winging It” are high-risk strategies and very few people can pull it off. 
                  Have a very clear roadmap of what you’re going to say, and rehearse.  If you want to ad-lib a couple of stories, that’s fine, but be sure you know the key points so you don’t meander. 
                  Having your material down cold will enable you to have more fun with your audience and avoid the nervousness associated with being not quite ready.
                  </p>
                  <input className="checkb" type="checkbox" id="checkbox" />
                  <label for="checkbox"></label>
                </p>
              </div>
            </li>
            <li className="in-view">
              <div className="reveal">
                <p>
                  <phase>Watch yourself.</phase> 
                  <p>Few tools are as instructive as video playback. 
                  People can tell you that you wander the stage, over-gesture, slouch, have an incessantly grim facial expression or use a repetitive speech pattern, but once you see it on tape, it will be much easier for you to grasp and change. 
                  If you prefer to rehearse in private, use your iPad or hand-held device’s video feature.
                   Stand in front of it and let it roll!  
                   </p>   
                   <input className="checkb" type="checkbox" id="checkbox" />
                  <label for="checkbox"></label>

                 </p>
              </div>
            </li>
            <li className="in-view">
              <div className="reveal">
                <p >
                  <phase>Avoid sameness. </phase> 
                  <p>   It is said that sameness is the enemy of speaking.
                   If you follow the same cadence, vocal rhythm, pitch, tone and gesture patterns throughout your presentation, your audience will tune you out.
                    Think about what puts a baby to sleep.
                   You need to change it up; keep enough variety in your delivery so it holds the audience’s interest.
                   </p>
                   <input className="checkb" type="checkbox" id="checkbox" />
                  <label for="checkbox"></label>

                </p>      
              </div>
            </li>
            <li className="in-view">
              <div className="reveal">
                <p >
                  <phase>Message your body.</phase> 
                 <p> Remember that 90+% of communication is nonverbal. Your audience will read your facial expressions, the tone of your voice, the way you use your hands, how you stand and move. 
                      A warm, easy smile and calm body immediately tell the audience that you’re comfortable and confident.
                      And when the speaker is comfortable, the audience is, too.
                      The opposite is also true.
                 </p> 
                 <input className="checkb" type="checkbox" id="checkbox" />
                  <label for="checkbox"></label>

                </p>      
              </div>
            </li>
            <li className="in-view">
              <div className="reveal">
                <p >
                  <phase>Let your passion show. </phase> 
                 <p>
                  There is no substitute for authentic passion at the podium.
                   When you believe in your message and have energy around your topic, it will translate to your audience.
                    Above all else, be yourself up there!
                  </p> 
                  <input className="checkb" type="checkbox" id="checkbox" />
                  <label for="checkbox"></label>
     
                </p>
              </div>
            </li>

          </ul>
        </section>
        <Footer />
        </div>
      );
    }
  }
  export default singlemap;