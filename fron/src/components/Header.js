import React, { Component } from 'react';
import '../styles/Header.css'
import { NavLink } from 'react-router-dom'


class Header extends Component {
  render() {
    return (

      <div className="navbar">
        <ul className="nav">
          <li className= "nav-item" >
            <NavLink to='/'> <i className="icon-fixed-width icon-home" /> Home </NavLink>
          </li>
          <li className="nav-item">
          <NavLink  to='/profilepage'> <i className="icon-fixed-width icon-user" /> My Profile </NavLink>

            </li>
          <li className="nav-item"><a href="#"><i className="icon-fixed-width icon-envelope" />contact</a></li>

                    <li className="nav-item" id="signout">
          <NavLink  to='/'> <i className="icon-fixed-width icon-user" /> Sign Out </NavLink>
          </li>

        </ul>
      </div>
    );
  }
}
    export default Header;