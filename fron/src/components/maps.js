import React, { Component } from 'react';
import './../styles/maps.css';
import Footer from './Footer'
import Header from './Header'
import { Link } from 'react-router-dom'


class maps extends Component {
  state={
    maps_list:[],
   
  };


  getmaplist = async order => {
    try {
      const response = await fetch(
        `http://localhost:8080/maps/list?order=${order}`
      );
  
      const answer = await response.json();
      if (answer) {
        const maps_list = answer.result;
        this.setState({ maps_list });
  
      } else {
        this.setState({ error_message: answer.message });
  
      }
    } catch (err) {
      this.setState({ error_message: err.message });
  
    }
  };

  componentDidMount() {
    //this.getmemberslist();
    this.getmaplist();
    window.scrollTo(0, 0)
  }

  
  render() {

  
    return (
      <div className="mapscards">
        <Header></Header>

        <title>Learnings Roadmaps</title>
     
        <div className="logocontainer">
          <img className="logo" src={require('../logo/WithS.png')} />
        </div>


          <Link class="btn" data-js="btn" to='./createmap'>
            <span class="btn-inr">
           <span class="txt-a">Create your learning map</span>
             <span class="txt-b">Start Your Journey</span>
          </span>
          </Link>

        {/* Main */}
        <div id="main">
          <div className="inner">
            {/* Boxes */}
            <div className="thumbnails">
              <div className="box">
              <a href="/" className="image fit"><img src="https://cms-assets.tutsplus.com/uploads/users/988/posts/31255/image/What-is-public-speaking%20(1).jpg" alt /></a>
                <div className="inner">
                  <h3>Want To Be A Better Public Speaker?</h3>
                  Do you dread public speaking? Join the club. Along with death and spiders, it's what people fear most. However, being an effective presenter is critical for anyone who is (or aspires to be) in a leadership position.
                                    <br />
                  <br />
                  <br />

                  
                  <Link className="button fit" to='./mapview' >view</Link>
                  <Link className="button fit" to='./profilepage' >Fork</Link>

                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPv0eMrqMP6Y5TzBNM794lhpzhBTQEMv26b9p0EsFsBICJl2Xw" alt /></a>
                <div className="inner">
                  <h3>How to learn writing Essays</h3>
                  <p>Academic essay writing is a style that anyone can learn to produce, once they know the basics of writing an essay. An academic essay should provide a solid, debatable thesis that is then supported by relevant evidence—whether that be from other sources or from one's own research.
                    </p>
                  <Link className="button fit" to='./mapview' >view</Link>
                  <Link className="button fit" to='./profilepage' >Fork</Link>
                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img src="https://d1aeri3ty3izns.cloudfront.net/media/11/119190/1200/preview.jpg" alt /></a>
                <div className="inner">
                  <h3>Simple Steps to play guitar</h3>
                  <p>
                  For some players, learning to play guitar by themselves is really the only way to go. 
                  Guitar lessons have their place, and there is no substitute for an experienced teacher or mentor who is committed to helping you reach your goals. 
                  </p>
                  <br/>
                  <Link className="button fit" to='./mapview' >view</Link>
                  <Link className="button fit" to='./profilepage' >Fork</Link>
                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img src="https://www.internationalrelationsedu.org/wp-content/uploads/2018/05/languages.jpg?x70247" alt /></a>
                <div className="inner">
                  <h3>How To Speak Any Language Simple Steps</h3>
                  <p>
                  So you want to become fluent in a second, third or maybe even a fourth language?

There are tons of language learning materials out there and you can find a lot of  language learning advice online.
                   </p>
                  <Link className="button fit" to='./mapview' >view</Link>
                  <Link className="button fit" to='./profilepage' >Fork</Link>
                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRe9ce3MuntD9dRQc_EZQwIXolgAHKDqNTQ0itvB1rm1NSxVXa_MQ" alt /></a>
                <div className="inner">
                  <h3>Photography for beginners</h3>
                  <p>
                  Cameras are complicated. I was frustrated with my first DSLR. I just couldn’t capture what I saw through my viewfinder. It took a ton of trial and error.
                  When I managed to work it all out, I started taking some pretty spectacular images.
                  </p>
                  <Link className="button fit" to='./mapview' >view</Link>
                  <Link className="button fit" to='./profilepage' >Fork</Link>
                </div>
              </div>
              <div className="box">
              <a href="/" className="image fit"><img src="https://cdn-images-1.medium.com/max/1200/1*mYnT8uj0ukTlFY_1zP-bWQ.png" alt /></a>
                <div className="inner">
                  <h3>Steps to pass the interview</h3>
                  <p>
                  Do you know how to make your case to an interviewer? Follow these 10 interview tips to boost your chances of landing the job.
                   Let's see How!!.
                   </p>
                   <br/>
                   <br/>
                   <br />

                  <Link className="button fit" to='./mapview' >view</Link>
                  <Link className="button fit" to='./profilepage' >Fork</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer ></Footer>

      </div>
    );
  }
}
 


export default maps;