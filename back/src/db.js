import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
  /**
     * Create the table
     **/
/*   await db.run(`CREATE TABLE `user_profile` (
	`user_id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`user_name`	text NOT NULL,
	`user_email`	text NOT NULL,
	`user_career`	text NOT NULL,
	`user_bio`	text NOT NULL,
	`user_stars`	integer NOT NULL,
	`user_image`	text NOT NULL,
	`user_facebook` text,
	`user_linkedin` text, 
	`user_twitter` text,
  `user_verfied` text NOT NULL
  `user_numberofmaps` text

););

  await db.run(`CREATE TABLE learn_map (
  map_id integer NOT NULL CONSTRAINT learn_map_pk PRIMARY KEY,
  map_title text NOT NULL,
  map_description clob NOT NULL,
  map_public boolean NOT NULL,
  user_profile_user_id integer NOT NULL,
  map_fork_id integer NOT NULL,
  map_tags text NOT NULL,
  CONSTRAINT learn_map_user_profile FOREIGcreatenewmapN KEY (user_profile_user_id)
  REFERENCES user_profile (user_id)
);`);

  await db.run(`CREATE TABLE learn_map_item (
  item_id integer NOT NULL CONSTRAINT learn_map_item_pk PRIMARY KEY,
  item_title text NOT NULL,
  item_description clob NOT NULL,
  item_image text NOT NULL,
  item_checked boolean NOT NULL,
  user_profile_user_id integer NOT NULL,
  learn_map_map_id integer NOT NULL,
  CONSTRAINT learn_map_item_user_profile FOREIGN KEY (user_profile_user_id)
  REFERENCES user_profile (user_id),
  CONSTRAINT learn_map_item_learn_map FOREIGN KEY (learn_map_map_id)
  REFERENCES learn_map (map_id)
);`); */



  // user profile controllers


  // VIEW USER PROFILE 

  const viewuserprofile = async (id) => {
    let returnString = ""
    const rows = await db.all(`SELECT user_id AS id, user_name, user_career, user_bio, user_stars, user_numberofmaps, user_image, user_facebook, user_linkedin, user_twitter, user_verfied FROM user_profile WHERE user_id = ${id}`)
    return rows
    rows.forEach( ({ id, user_name, user_career, user_bio, user_stars, user_numberofmaps, user_image, user_facebook, user_linkedin, user_twitter, user_verfied }) => returnString+=`[id:${id}] - ${user_name} - ${user_career} - ${user_bio} - ${user_stars} - ${user_numberofmaps} - ${user_image} - ${user_facebook} - ${user_linkedin} - ${user_twitter} - ${user_verfied}` )
    return returnString
  }
// CREATE USER PROFILE

  const createuserprofile = async (props) => {
    if(!props || !props.user_name || !props.user_email || !props.user_bio){
      throw new Error(`you must provide a name and an email and bio and`)
    }

    
    const {user_career, user_name, user_email, user_bio, user_image, user_twitter, user_facebook, user_linkedin } = props
    try{
      const result = await db.run(SQL`INSERT INTO user_profile (user_name,user_email,user_career,user_bio,user_image,user_facebook,user_linkedin,user_twitter) VALUES (${user_name}, ${user_email}, ${user_career}, ${user_bio}, ${user_image}, ${user_facebook}, ${user_linkedin}, ${user_twitter})`);
      const id = result.stmt.lastID
      return id

    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
// UPDATE USER PROFILE

  const updateuserprofile = async (id, props) => {
    const  { user_name, user_email, user_career, user_bio, user_image, user_facebook, user_linkedin, user_twitter } = props
    const result = await db.run(SQL`UPDATE user_profile SET user_name=${user_name}, user_email=${user_email}, user_image=${user_image}, user_bio=${user_bio}, user_career=${user_career}, user_facebook=${user_facebook}, user_twitter=${user_twitter}, user_linkedin=${user_linkedin} WHERE user_id = ${id}`);
    if(result.stmt.changes === 0){
      return false
    }
    return true
  }
  
  
// Map controllers 

// GET MAP LIST

  const getmaplist = async () => {
    let returnString = ""
    const rows = await db.all("SELECT map_id AS id, map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags FROM learn_map")
    return rows
    rows.forEach( ({ id, map_title, map_description, map_public, user_profile_user_id, map_fork_id, map_tags }) => returnString+=`[id:${id}] - ${map_title} - ${map_description} - ${map_public} - ${user_profile_user_id} - ${map_fork_id} - ${map_tags}` )
    return returnString
  }

  // VIEW A MAP

  const viewmap = async (map_id) => {
    let returnString = ""
    const rows = await db.all(`SELECT map_id AS id, map_title, map_description, map_image, map_tags FROM learn_map WHERE map_id = ${map_id}`)
    return rows
    rows.forEach( ({ id, map_title, map_description, map_image }) => returnString+=`[id:${id}] - ${map_title} - ${map_description} - ${map_image}` )
    return returnString
  }


  // CREATE NEW MAP

  const createnewmap = async (props) => {
    if(!props || !props.map_title || !props.map_description){
      throw new Error(`you must provide a title and  description`)
    }

  
    console.log(props);
    const {map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags } = props
    
    const result = await db.run(SQL`INSERT INTO learn_map (map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags) VALUES (${map_title}, ${map_description}, ${map_public}, ${map_image}, ${user_profile_user_id}, ${map_fork_id}, ${map_tags})`);
    
    const newmap_id = result.stmt.lastID
    
    
    return newmap_id 
       
    }
// FORK A MAP

    const forkmap = async (x) => {  
      //get the map that I want to fork
      const {user_id, map_id} = x;
      console.log(map_id)
        const map =  await db.all(`SELECT * FROM learn_map WHERE map_id = ${map_id}`);
        console.log(map)
        //get the items that are part of the map I want to fork
        const items = await db.all(`SELECT * FROM learn_map_item WHERE learn_map_map_id = ${map_id}`);
        const maptoFork = map[0];
        //const {item_title, item_description, item_image, learn_ map_map_id} = itemtofork;
        let {map_title, map_description, map_public, map_image, map_fork_id, map_tags} = maptoFork;
        console.log(map_title, map_description, map_public, map_image, map_fork_id, map_tags)
        map_fork_id = map_fork_id +1;

        //create a new map based on the one I retrieved (maptoFork)
        const user_profile_user_id = user_id
        const new_map_id = await createnewmap({map_title, map_description, map_public,map_image, user_profile_user_id , map_fork_id, map_tags})
        const learn_map_map_id = new_map_id;
        const item_checked = false;
        //create new items linked to the map I created above by copying each item in the items I retrieved
        for( let i=0; i< items.length;i++){
          const {item_title, item_description, item_image } = items[i]
          const new_item_id = await createnewmapitem({item_title, item_description, item_image,item_checked, user_profile_user_id, learn_map_map_id })
          item[i].id = new_item_id
        }
        
        return items

      }
    
      
  
// UPDATE LEARN MAP

    const updatelearnmap = async (id, props) => {
      const  { map_title, map_description,map_image, map_public, map_tags,} = props
      const result = await db.run(SQL`UPDATE learn_map SET map_title=${map_title}, map_description=${map_description}, map_image=${map_image}, map_tags=${map_tags}, map_public=${map_public}  WHERE map_id = ${id}`);
      if(result.stmt.changes === 0){
        return false
      }
      return true
    }

    const deletelearnmap = async (id) => {
      const result = await db.run(SQL`DELETE FROM learn_map WHERE map_id = ${id}` );
      if(result.stmt.changes === 0){
        return false
      }
      return true
  
    }

// SET MAP TO PUBLIC

    const setmappublic = async (id, props) => {
      const  { map_public} = props
      const result = await db.run(SQL`UPDATE learn_map SET publicview=${map_public}  WHERE map_id = ${id}`);
      if(result.stmt.changes === 0){
        return false
      }
      return true
    }



    
    
 


  // Map items controllers

  const createnewmapitem = async (props) => {
    if(!props || !props.item_title || !props.item_description){
      throw new Error(`you must provide a title and  description`)
    }

  
    console.log(props);
    const { item_title, item_description, item_checked, item_image, learn_map_map_id, user_profile_user_id } = props
    
    const result = await db.run(SQL`INSERT INTO learn_map_item (item_title, item_description, item_checked, item_image, learn_map_map_id, user_profile_user_id) VALUES (${item_title}, ${item_description}, ${item_checked}, ${item_image}, ${learn_map_map_id}, ${user_profile_user_id})`);
    
    const newmap_id = result.stmt.lastID
    
    return newmap_id 
    }


  const viewmapitem = async (item_id) => {
    let returnString = ""
    const rows = await db.all(`SELECT item_id AS id, item_title, item_description, item_image FROM learn_map_item WHERE item_id =${item_id}`)
    return rows
    rows.forEach( ({ id, item_title, item_description, item_image }) => returnString+=`[id:${id}] - ${item_title} - ${item_description} - ${item_image}` )
    return returnString
  }

  const listmapitems = async (id) => {
    let returnString = ""
    const rows = await db.all("SELECT item_id AS id, item_title, item_description, item_image FROM learn_map_item")
    return rows
    rows.forEach( ({ id, item_title, item_description, item_image }) => returnString+=`[id:${id}] - ${item_title} - ${item_description} - ${item_image}` )
    return returnString
  }


  const deletemapitem = async (id) => {
    const result = await db.run(SQL`DELETE FROM learn_map_item WHERE item_id = ${id}` );
    if(result.stmt.changes === 0){
      return false
    }
    return true

  }

  const updatemapitem = async (id, props) => {
    const  { item_title, item_description, item_image, } = props
    const result = await db.run(SQL`UPDATE learn_map_item SET item_title=${item_title}, item_description=${item_description}, item_image=${item_image}  WHERE item_id = ${id}`);
    if(result.stmt.changes === 0){
      return false
    }
    return true
  }

  const checkmapitem = async (id, props) => {
    const  { item_checked, } = props
    const result = await db.run(SQL`UPDATE learn_map_item SET item_check=${item_checked} WHERE map_id = ${id}`);
    if(result.stmt.changes === 0){
      return false
    }
    return true
  }





  const controller = {
    viewuserprofile,
    getmaplist,
    viewmapitem,
    createuserprofile,
    updateuserprofile,
    createnewmap,
    forkmap,
    createnewmapitem,
    updatelearnmap,
    deletelearnmap,
    deletemapitem,
    updatemapitem,
    checkmapitem,
    setmappublic,
    viewmap,
    listmapitems
  }

  return controller
}




  


export default initializeDatabase
